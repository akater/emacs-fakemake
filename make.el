;;;  -*- lexical-binding: t -*-

;; Don't litter.
(setq backup-by-copying t
      backup-directory-alist `(("." . ,(expand-file-name
                                        ".saves" user-emacs-directory)))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control nil)

;; This is likely only needed when org had been ripped off Emacs
;; and installed separately.
(require 'org)
(require 'org-element)
(setq org-element-cache-persistent nil)
;; Silence some messages.
(defalias 'org-development-elisp-mode 'org-mode)
(defvar ob-flags)
(defvar bootstrap nil)
(defun fakemake-code-path (component)
  (unless (memq component '( fakemake
                             fakemake-def fakemake-buildenv fakemake-done
                             init install))
    (error "Unsupported fakemake component: %s" component))
  (expand-file-name (format "%s.el" component)
                    (if bootstrap "fakemake/" "etc/fakemake/")))
(require 'cl-macs)
(defun fakemake--org-eval-named-blocks-in (file &rest names)
  (declare (indent 1))
  (with-current-buffer (find-file-noselect file)
    (let (org-confirm-babel-evaluate)
      (save-excursion
        (dolist (name names)
          (org-babel-goto-named-src-block name)
          (org-babel-execute-src-block))))))
(fakemake--org-eval-named-blocks-in "fakemake.org"
  "defmacro npush-pop"
  ;; The above is a dependency of the below.
  "defvar fakemake-original-files")

(defvar fakemake-bootstrap-makefile-name "Makefile.bootstrapped")
(let ((bootstrap t) ob-flags)
  (org-babel-tangle-file "fakemake.org"))

(add-to-list 'load-path (expand-file-name "fakemake"))
(load "init" nil t)
(load "fakemake" nil t)
(make-symbolic-link fakemake-bootstrap-makefile-name "Makefile" t)
(fakemake nil t)
